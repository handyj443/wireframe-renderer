// HEADER file
#ifndef MATH_H
#define MATH_H

#include <xmmintrin.h>

__declspec(align(16))
struct Vec4
{
	// constructors
	Vec4() : x(0), y(0), z(0), w(0) { }
	Vec4(const float x, const float y, const float z, const float w)
		: x(x), y(y), z(z), w(w) { }

	// functions
	Vec4 operator-(const Vec4 &rhs);

	// data
	union
	{
		struct
		{
			float x, y, z, w;
		};
		__m128 ps;  // packed singles
	};
};

__declspec(align(16))
struct Mat4
{
	// data
	union
	{
		float m[4][4];
		__m128 ps[4];  // 4 rows
	};
};

Vec4 Vec3Normalize(const Vec4 &v);
float Vec3Dot(const Vec4 &a, const Vec4 &b);
Vec4 Vec3Cross(const Vec4 &a, const Vec4 &b);
Vec4 Vec3Transform(const Vec4 &v, const Mat4 &m);
// never actually used...
//void Mat4Multiply(Mat4 *pResult, const Mat4 &a, const Mat4 &b);
float ToRad(float angleDeg);

#endif