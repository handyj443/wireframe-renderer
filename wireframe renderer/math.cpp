// CPP file
#include "math.h"
#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>
#include <smmintrin.h>
#include <cmath>  // for the sqrt

using uint = unsigned int;

// CONSTANTS
// Control bits for dp_ps intrinsic
// High 4 bits select which components to dot product. Low 4 bits select which 
// destination fields to broadcast result to. 
const uint dppsBits = 0b01111111;

Vec4 Vec4::operator-(const Vec4 &rhs)
{
#ifdef ASM_MATHS
	Vec4 diff = *this;
	diff.ps = _mm_sub_ps(diff.ps, rhs.ps);
	return diff;

#else
	Vec4 diff = *this;
	diff.x -= rhs.x;
	diff.y -= rhs.y;
	diff.z -= rhs.z;
	diff.w -= rhs.w;
	return diff;
#endif
}


Vec4 Vec3Normalize(const Vec4 &v)
{
#ifdef ASM_MATHS
	// square and sum x, y, z components and broadcast to all 4 fields of the destination
	__m128 length = _mm_dp_ps(v.ps, v.ps, dppsBits);  
	length = _mm_sqrt_ps(length);  // square root to get length

	Vec4 ret;
	ret.ps = _mm_div_ps(v.ps, length);  // normalise v
	ret.w = v.w;  // copy over w component
	return ret;

#else
	Vec4 ret;
	float length = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
	ret.x = v.x / length;
	ret.y = v.y / length;
	ret.z = v.z / length;
	ret.w = v.w;
	return ret;
#endif
}

float Vec3Dot(const Vec4 &a, const Vec4 &b)
{
#ifdef ASM_MATHS
	// square and sum x, y, z components and broadcast to all 4 fields of the destination
	__m128 ret = _mm_dp_ps(a.ps, b.ps, dppsBits);
	return ret.m128_f32[0];  // return first float of xmm word
	
#else
	return a.x * b.x + a.y * b.y + a.z * b.z;
#endif
}

Vec4 Vec3Cross(const Vec4 &a, const Vec4 &b)
{
#ifdef ASM_MATHS
	// a.y, a.z, a.x
	__m128 a1 = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(a.ps), 0b11001001));
	// b.z, b.x, b.y
	__m128 b1 = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(b.ps), 0b11010010));
	// a.z, a.x, a.y
	__m128 a2 = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(a.ps), 0b11010010));
	// b.y, b.z, b.x
	__m128 b2 = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(b.ps), 0b11001001));

	Vec4 ret;
	// ((a.y, a.z, a.x) * (b.z, b.x, b.y)) - ((a.z, a.x, a.y) * (b.y, b.z, b.x))
	ret.ps = _mm_sub_ps(_mm_mul_ps(a1, b1), _mm_mul_ps(a2, b2));
	ret.w = 0.0;  // set w to 0
	return ret;

#else
	Vec4 ret;
	ret.x = a.y * b.z - a.z * b.y;
	ret.y = a.z * b.x - a.x * b.z;
	ret.z = a.x * b.y - a.y * b.x;
	ret.w = 0.0;
	return ret;
#endif
}

Vec4 Vec3Transform(const Vec4 &v, const Mat4 &mat)
{
#ifdef ASM_MATHS;
	Vec4 ret;
	__m128 x = _mm_load1_ps(&v.x);
	__m128 y = _mm_load1_ps(&v.y);
	__m128 z = _mm_load1_ps(&v.z);
	__m128 w = _mm_load1_ps(&v.w);

	ret.ps = _mm_mul_ps(x, mat.ps[0]);
	ret.ps = _mm_add_ps(ret.ps, _mm_mul_ps(y, mat.ps[1]));
	ret.ps = _mm_add_ps(ret.ps, _mm_mul_ps(z, mat.ps[2]));
	ret.ps = _mm_add_ps(ret.ps, _mm_mul_ps(w, mat.ps[3]));

	return ret;

#else
	Vec4 ret;
	ret.x = v.x * mat.m[0][0] + v.y * mat.m[1][0] + v.z * mat.m[2][0] +
		v.w * mat.m[3][0];
	ret.y = v.x * mat.m[0][1] + v.y * mat.m[1][1] + v.z * mat.m[2][1] +
		v.w * mat.m[3][1];
	ret.z = v.x * mat.m[0][2] + v.y * mat.m[1][2] + v.z * mat.m[2][2] +
		v.w * mat.m[3][2];
	ret.w = v.x * mat.m[0][3] + v.y * mat.m[1][3] + v.z * mat.m[2][3] +
		v.w * mat.m[3][3];
	return ret;
#endif
}

//void Mat4Multiply(Mat4 *pResult, const Mat4 &a, const Mat4 &b)
//{
//	pResult->m[0][0] = a.m[0][0] * b.m[0][0] + a.m[0][1] * b.m[1][0] +
//		a.m[0][2] * b.m[2][0] + a.m[0][3] * b.m[3][0];
//	pResult->m[0][1] = a.m[0][0] * b.m[0][1] + a.m[0][1] * b.m[1][1] +
//		a.m[0][2] * b.m[2][1] + a.m[0][3] * b.m[3][1];
//	pResult->m[0][2] = a.m[0][0] * b.m[0][2] + a.m[0][1] * b.m[1][2] +
//		a.m[0][2] * b.m[2][2] + a.m[0][3] * b.m[3][2];
//	pResult->m[0][3] = a.m[0][0] * b.m[0][3] + a.m[0][1] * b.m[1][3] +
//		a.m[0][2] * b.m[2][3] + a.m[0][3] * b.m[3][3];
//	pResult->m[1][0] = a.m[1][0] * b.m[0][0] + a.m[1][1] * b.m[1][0] +
//		a.m[1][2] * b.m[2][0] + a.m[1][3] * b.m[3][0];
//	pResult->m[1][1] = a.m[1][0] * b.m[0][1] + a.m[1][1] * b.m[1][1] +
//		a.m[1][2] * b.m[2][1] + a.m[1][3] * b.m[3][1];
//	pResult->m[1][2] = a.m[1][0] * b.m[0][2] + a.m[1][1] * b.m[1][2] +
//		a.m[1][2] * b.m[2][2] + a.m[1][3] * b.m[3][2];
//	pResult->m[1][3] = a.m[1][0] * b.m[0][3] + a.m[1][1] * b.m[1][3] +
//		a.m[1][2] * b.m[2][3] + a.m[1][3] * b.m[3][3];
//	pResult->m[2][0] = a.m[2][0] * b.m[0][0] + a.m[2][1] * b.m[1][0] +
//		a.m[2][2] * b.m[2][0] + a.m[2][3] * b.m[3][0];
//	pResult->m[2][1] = a.m[2][0] * b.m[0][1] + a.m[2][1] * b.m[1][1] +
//		a.m[2][2] * b.m[2][1] + a.m[2][3] * b.m[3][1];
//	pResult->m[2][2] = a.m[2][0] * b.m[0][2] + a.m[2][1] * b.m[1][2] +
//		a.m[2][2] * b.m[2][2] + a.m[2][3] * b.m[3][2];
//	pResult->m[2][3] = a.m[2][0] * b.m[0][3] + a.m[2][1] * b.m[1][3] +
//		a.m[2][2] * b.m[2][3] + a.m[2][3] * b.m[3][3];
//	pResult->m[3][0] = a.m[3][0] * b.m[0][0] + a.m[3][1] * b.m[1][0] +
//		a.m[3][2] * b.m[2][0] + a.m[3][3] * b.m[3][0];
//	pResult->m[3][1] = a.m[3][0] * b.m[0][1] + a.m[3][1] * b.m[1][1] +
//		a.m[3][2] * b.m[2][1] + a.m[3][3] * b.m[3][1];
//	pResult->m[3][2] = a.m[3][0] * b.m[0][2] + a.m[3][1] * b.m[1][2] +
//		a.m[3][2] * b.m[2][2] + a.m[3][3] * b.m[3][2];
//	pResult->m[3][3] = a.m[3][0] * b.m[0][3] + a.m[3][1] * b.m[1][3] +
//		a.m[3][2] * b.m[2][3] + a.m[3][3] * b.m[3][3];
//}

float ToRad(float angleDeg)
{
	return 3.14159265359f * angleDeg / 180;
}