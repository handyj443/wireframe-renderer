#include "math.h"

int main_testing()
{
	Vec4 a(1, 2, 3, 4);
	Vec4 b(2, 3, 4, 5);

	Vec4 c = b - a;

	Vec4 d = Vec3Normalize(Vec4(2, 3, 6, 1));

	Vec4 e = Vec3Cross(a, b);

	return 0;
}