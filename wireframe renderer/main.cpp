/* DirectX convention: left handed coordinate system, row vectors premultiply
   with matrices. Z is up in world space. */
#include <Windows.h>
#include <Windowsx.h>
#include <cstdio>
#include <cstring>
#include <sstream>
#include <cmath>
#include <cfloat>
#include "math.h"
#include "GameTimer.h"

// UNIT TESTING MATHS FUNCTIONS
#include "main_testing.h"

   // PREPROCESSOR FLAGS
#define ASSERTIONS_ENABLED 0

// MACROS
#if ASSERTIONS_ENABLED
	#define Assert(Expression)                                                     \
		if (Expression) {                                                          \
			;                                                                      \
		} else {                                                                   \
			*(int *)0 = 0;                                                         \
		}
#else
	#define Assert(Expression)
#endif

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

// UNIT TESTING MATHS FUNCTIONS
#ifdef _DEBUG
	#define UNIT_TEST
#endif

#define PI 3.14159265359
#define TAU 6.28318530718
typedef UINT8 uint8;
typedef UINT32 uint32;
typedef unsigned uint;

using namespace std;
//______________________________________________________________________________
// GLOBAL DATA STRUCTURES
struct Tri
{
	uint a, b, c;
};
struct Quad
{
	uint a, b, c, d;
};
struct ScreenBuffer
{
	BITMAPINFO info;
	void *pMemory;
	uint width;
	uint height;
	uint pitch; // width*bytesPerPixel
	uint bytesPerPixel;
};
struct Color
{
	uint8 r, g, b, a;
};
struct Model
{
	uint nVerts;
	uint nFaces;
	uint nTris;
	uint nEdges;
	Vec4 *pVerts;
	float minX, maxX;
	float minY, maxY;
	float minZ, maxZ;
	Tri *pTris;
};
struct Camera
{
	Vec4 position;
	Vec4 target;
	Vec4 upDirection;
	float l;
	float r;
	float t;
	float b;
	float n;
	float f;
	float aspect;
	float vFovDeg;
};
//______________________________________________________________________________
// GLOBAL VARIABLES
//char *modelPath = "models/test.off";
char *modelPath = "models/space_station.off";
//char *modelPath = "models/teapot.off";
//char *modelPath = "models/apple.off";
//char *modelPath = "models/house.off";
//char *modelPath = "models/helm.off";
HWND ghMainWnd;
uint gViewWidth = 1024;
uint gViewHeight = 786;
ScreenBuffer gSBuffer;
Camera gCamera;
bool gbDrawPixels = false;
bool gbDrawTris = true;
Model gModel;
Vec4 *gpVBuffer;
bool gbMouseSpin = false; // When true, moving the mouse rotates the model
short gMouseX;
short gMouseY;
short gPrevMouseX;
short gPrevMouseY;
//______________________________________________________________________________
// FORWARD DECLARATIONS
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
void RecreateScreenBuffer(ScreenBuffer *buffer, uint width, uint height);
void Render(ScreenBuffer *pBuffer, Vec4 *pVBuffer, const Model &model);
void WritePixel(ScreenBuffer *pBuffer, uint i, uint j, Color color);
Color ShadePixel(uint i, uint j);
bool ReadOFFModel(const char *filename, Model *model);
void MatrixLookAt(Mat4 *pMatrix, Vec4 position, Vec4 target, Vec4 upDirection);
void MatrixPerspective(Mat4 *pmatPersp, const Camera &c);
void MatrixRotateY(Mat4 *pMatrix, float angleDeg);
void MatrixRotateZ(Mat4 *pMatrix, float angleDeg);
void ClearBuffer(ScreenBuffer *pBuffer);
void SetupCamera(Camera *camera, float vFovDeg, Vec4 pos, Vec4 target, Vec4 up,
				 float aspect, float n, float f);
void RotateCamera(Camera *pCamera, float aziDeg, float polarDeg);
void DrawLine(ScreenBuffer *pSBuffer, const Vec4 &a, const Vec4 &b);
void DrawTri(ScreenBuffer *pSBuffer, const Vec4 &a, const Vec4 &b,
			 const Vec4 &c);
void CalculateFrameStats(GameTimer &timer);
//______________________________________________________________________________
// MAIN PROCEDURE
int WINAPI
WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR pCmdLine, int nShow)
{
#ifdef UNIT_TEST
	main_testing();
	return 0;
#else
	// Create and register window class, and create window
	WNDCLASS wc;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(0, IDI_APPLICATION);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = 0;
	wc.lpszClassName = L"WireframeRenderer";
	if (!RegisterClass(&wc))
	{
		MessageBox(0, L"RegisterClass FAILED", 0, 0);
		return -1;
	}

	ghMainWnd = CreateWindow(L"WireframeRenderer", L"Wireframe Renderer",
							 WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
							 gViewWidth, gViewHeight, 0, 0, hInstance, 0);
	if (ghMainWnd == 0)
	{
		MessageBox(0, L"CreateWindow FAILED", 0, 0);
		return -1;
	}
	ShowWindow(ghMainWnd, SW_SHOW);

	// Read in model data
	if (!ReadOFFModel(modelPath, &gModel))
	{
		MessageBox(0, L"fopen FAILED", 0, 0);
		return -1;
	}
	gpVBuffer = new Vec4[gModel.nVerts];

	// Create the screen buffer
	RecreateScreenBuffer(&gSBuffer, gViewWidth, gViewHeight);
	// Initial camera setup
	float modelMidX = (gModel.maxX + gModel.minX) / 2;
	float modelMidY = (gModel.maxY + gModel.minY) / 2;
	float modelMidZ = (gModel.maxZ + gModel.minZ) / 2;
	Vec4 camPosition = { modelMidX, gModel.maxY * 5, modelMidZ, 1 };
	Vec4 camTarget = { modelMidX, modelMidY, modelMidZ, 1 };
	SetupCamera(&gCamera, 45.0, camPosition, camTarget, { 0, 0, 1, 0 },
				(float)gViewWidth / gViewHeight, 1, 1000);

	// Enter the message processing loop
	GameTimer timer;
	timer.Reset();
	timer.Start();
	MSG msg = {};
	BOOL bRet = 1;
	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			timer.Tick();
			CalculateFrameStats(timer);
			Render(&gSBuffer, gpVBuffer, gModel);
			RedrawWindow(ghMainWnd, 0, 0, RDW_INVALIDATE);
		}
	}
	return (int)msg.wParam;
#endif
}

//______________________________________________________________________________
// MESSAGE HANDLING
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{

		case WM_MOUSEMOVE:
			gMouseX = GET_X_LPARAM(lParam);
			gMouseY = GET_Y_LPARAM(lParam);
			if (gbMouseSpin)
			{
				RotateCamera(&gCamera, (float)gMouseX - gPrevMouseX,
							 (float)gMouseY - gPrevMouseY);
				gPrevMouseX = gMouseX;
				gPrevMouseY = gMouseY;
			}
			return 0;

		case WM_LBUTTONDOWN:
			if (gbMouseSpin)
			{
				gbMouseSpin = false;
			}
			else
			{
				gbMouseSpin = true;
				gPrevMouseX = gMouseX;
				gPrevMouseY = gMouseY;
			}
			return 0;

		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;

		case WM_SIZE:
			gViewWidth = LOWORD(lParam);
			gViewHeight = HIWORD(lParam);
			RecreateScreenBuffer(&gSBuffer, gViewWidth, gViewHeight);
			SetupCamera(&gCamera, gCamera.vFovDeg, gCamera.position, gCamera.target,
						gCamera.upDirection, (float)gViewWidth / gViewHeight,
						gCamera.n, gCamera.f);
			Render(&gSBuffer, gpVBuffer, gModel);
			return 0;

		case WM_PAINT:
			PAINTSTRUCT paint;
			HDC dc = BeginPaint(hWnd, &paint);
			StretchDIBits(dc, 0, 0, gViewWidth, gViewHeight, 0, 0, gSBuffer.width,
						  gSBuffer.height, gSBuffer.pMemory, &gSBuffer.info,
						  DIB_RGB_COLORS, SRCCOPY);
			EndPaint(hWnd, &paint);
			return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

void CalculateFrameStats(GameTimer &timer)
{
	// Code computes the average frames per second, and also the
	// average time it takes to render one frame.  These stats
	// are appended to the window caption bar.
	static int frameCnt = 0;
	static float timeElapsed = 0.0f;
	frameCnt++;
	// Compute averages over period.
	const float TIMER_PERIOD = 2.0f;
	if ((timer.TotalTime() - timeElapsed) >= TIMER_PERIOD)
	{
		float fps = frameCnt / TIMER_PERIOD;
		float mspf = 1000.0f / fps;
		std::wostringstream outs;
		outs.precision(6);
		outs << "Wireframe Renderer" << L"    " << L"FPS: " << fps << L"    "
			<< L"Frame Time: " << mspf << L" (ms)";
		SetWindowText(ghMainWnd, outs.str().c_str());

		// Reset for next average.
		frameCnt = 0;
		timeElapsed += TIMER_PERIOD;
	}
}

//______________________________________________________________________________
// RENDERING
void RecreateScreenBuffer(ScreenBuffer *pBuffer, uint width, uint height)
{
	if (pBuffer->pMemory)
	{
		VirtualFree(pBuffer->pMemory, 0, MEM_RELEASE);
	}
	pBuffer->width = width;
	pBuffer->height = height;
	pBuffer->bytesPerPixel = 4;
	pBuffer->info.bmiHeader.biSize = sizeof(pBuffer->info.bmiHeader);
	pBuffer->info.bmiHeader.biWidth = pBuffer->width;
	pBuffer->info.bmiHeader.biHeight = -(int)pBuffer->height;
	pBuffer->pitch = pBuffer->width * pBuffer->bytesPerPixel;
	pBuffer->info.bmiHeader.biPlanes = 1;
	pBuffer->info.bmiHeader.biBitCount = 32;
	pBuffer->info.bmiHeader.biCompression = BI_RGB;

	int BitmapMemorySize =
		pBuffer->bytesPerPixel * pBuffer->width * pBuffer->height;
	pBuffer->pMemory = VirtualAlloc(0, BitmapMemorySize,
									MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
}

void SetupCamera(Camera *pCamera, float vFovDeg, Vec4 pos, Vec4 target, Vec4 up,
				 float aspect, float n, float f)
{
	pCamera->aspect = aspect;
	pCamera->vFovDeg = vFovDeg;
	pCamera->position = pos;
	pCamera->target = target;
	pCamera->upDirection = up;
	pCamera->n = n;
	pCamera->f = f;
	pCamera->t = tan(vFovDeg / 360 * (float)TAU / 2) * n;
	pCamera->b = -pCamera->t;
	pCamera->r = pCamera->t * aspect;
	pCamera->l = -pCamera->r;
}

// Rotate camera about its target by an azimuth and polar angle.
void RotateCamera(Camera *pCamera, float aziDeg, float polarDeg)
{
	Mat4 matRotate;
	MatrixRotateZ(&matRotate, aziDeg);
	pCamera->position = Vec3Transform(pCamera->position, matRotate);
}

void Render(ScreenBuffer *pSBuffer, Vec4 *pVBuffer, const Model &model)
{
	ClearBuffer(pSBuffer);
	// Calculate matrix to transform model from world space to camera space
	Mat4 viewMatrix;
	MatrixLookAt(&viewMatrix, gCamera.position, gCamera.target,
				 gCamera.upDirection);
	// Apply the view matrix
	for (uint t = 0; t < model.nVerts; ++t)
	{
		pVBuffer[t] = Vec3Transform(model.pVerts[t], viewMatrix);
	}
	// Calculate perspective transform matrix
	Mat4 perspMatrix;
	MatrixPerspective(&perspMatrix, gCamera);
	// Apply the perspective matrix
	for (uint t = 0; t < model.nVerts; ++t)
	{
		pVBuffer[t] = Vec3Transform(pVBuffer[t], perspMatrix);
	}
	// Perform the perspective divide.
	for (uint t = 0; t < model.nVerts; ++t)
	{
		pVBuffer[t].x /= pVBuffer[t].w;
		pVBuffer[t].y /= pVBuffer[t].w;
		pVBuffer[t].z /= pVBuffer[t].w;
		pVBuffer[t].w = 1.0;
		// Vertices should be in NDC
		Assert(abs(pVBuffer[t].x) <= 1.0);
		Assert(abs(pVBuffer[t].y) <= 1.0);
	}

	// Scale to viewport; viewport y axis is +ve downwards
	for (uint t = 0; t < model.nVerts; ++t)
	{
		pVBuffer[t].x = (pVBuffer[t].x + 1) / 2 * (gViewWidth - 1);
		pVBuffer[t].y =
			gViewHeight - (pVBuffer[t].y + 1) / 2 * (gViewHeight - 1);
		Assert(pVBuffer[t].x < gViewWidth);
		Assert(pVBuffer[t].y < gViewHeight);
	}
	// Display vertices as pixels
	if (gbDrawPixels)
	{
		Color color = { 255, 255, 255, 0 };
		for (uint t = 0; t < model.nVerts; ++t)
		{
			WritePixel(pSBuffer, (uint)round(pVBuffer[t].x),
					   (uint)round(pVBuffer[t].y), color);
		}
	}
	// Draw triangles with lines
	if (gbDrawTris)
	{
		for (uint t = 0; t < model.nTris; ++t)
		{
			DrawTri(pSBuffer, pVBuffer[model.pTris[t].a],
					pVBuffer[model.pTris[t].b], pVBuffer[model.pTris[t].c]);
		}
	}

	// DrawLine(pSBuffer, { 100, 500 }, { 100, 50 });
}

void inline DrawTri(ScreenBuffer *pSBuffer, const Vec4 &a, const Vec4 &b,
					const Vec4 &c)
{
	DrawLine(pSBuffer, a, b);
	DrawLine(pSBuffer, b, c);
	DrawLine(pSBuffer, c, a);
}

void inline DrawLine(ScreenBuffer *pSBuffer, const Vec4 &a, const Vec4 &b)
{
	int ax = (int)round(a.x);
	int ay = (int)round(a.y);
	int bx = (int)round(b.x);
	int by = (int)round(b.y);
	uint deltaX = abs(ax - bx);
	uint deltaY = abs(ay - by);
	if (deltaX > deltaY)
	{
		// Draw for each x
		if (ax > bx)
		{
			int tempx;
			int tempy;
			tempx = ax;
			ax = bx;
			bx = tempx;
			tempy = ay;
			ay = by;
			by = tempy;
		}
		float m = (by - ay) / (float)(bx - ax);
		Color color = { 255, 255, 255, 0 };
		for (uint t = 0; t <= deltaX; ++t)
		{
			WritePixel(pSBuffer, ax + t, (uint)((float)ay + m * t + 0.5),
					   color);
		}
	}
	else
	{
		// Draw for each y
		if (ay > by)
		{
			int tempx;
			int tempy;
			tempx = ax;
			ax = bx;
			bx = tempx;
			tempy = ay;
			ay = by;
			by = tempy;
		}
		float m = (bx - ax) / (float)(by - ay);
		Color color = { 255, 255, 255, 0 };
		for (uint t = 0; t <= deltaY; ++t)
		{
			WritePixel(pSBuffer, (uint)((float)ax + m * t + 0.5), ay + t,
					   color);
		}
	}
}

void inline ClearBuffer(ScreenBuffer *pSBuffer)
{
	memset(pSBuffer->pMemory, 0, pSBuffer->height * pSBuffer->pitch);
}

//______________________________________________________________________________
// VERTEX SHADER
// Camera space is u, v, w; u points to the right, v points up, camera is
// looking down the
// positive w axis.
void MatrixLookAt(Mat4 *pmatView, Vec4 position, Vec4 target,
				  Vec4 upDirection)
{
	Vec4 w = Vec3Normalize(target - position);
	Vec4 u = Vec3Normalize(Vec3Cross(upDirection, w));
	Vec4 v = Vec3Cross(w, u);
	pmatView->m[0][0] = u.x;
	pmatView->m[0][1] = v.x;
	pmatView->m[0][2] = w.x;
	pmatView->m[0][3] = 0.0;
	pmatView->m[1][0] = u.y;
	pmatView->m[1][1] = v.y;
	pmatView->m[1][2] = w.y;
	pmatView->m[1][3] = 0.0;
	pmatView->m[2][0] = u.z;
	pmatView->m[2][1] = v.z;
	pmatView->m[2][2] = w.z;
	pmatView->m[2][3] = 0.0;
	pmatView->m[3][0] = -Vec3Dot(position, u);
	pmatView->m[3][1] = -Vec3Dot(position, v);
	pmatView->m[3][2] = -Vec3Dot(position, w);
	pmatView->m[3][3] = 1.0;
}

void MatrixPerspective(Mat4 *pmatPersp, const Camera &c)
{
	pmatPersp->m[0][0] = 2 * c.n / (c.r - c.l);
	pmatPersp->m[0][1] = 0.0;
	pmatPersp->m[0][2] = 0.0;
	pmatPersp->m[0][3] = 0.0;
	pmatPersp->m[1][0] = 0.0;
	pmatPersp->m[1][1] = 2 * c.n / (c.t - c.b);
	pmatPersp->m[1][2] = 0.0;
	pmatPersp->m[1][3] = 0.0;
	pmatPersp->m[2][0] = -(c.r + c.l) / (c.r - c.l);
	pmatPersp->m[2][1] = -(c.t + c.b) / (c.t - c.b);
	pmatPersp->m[2][2] = c.f / (c.f - c.n);
	pmatPersp->m[2][3] = 1.0;
	pmatPersp->m[3][0] = 0.0;
	pmatPersp->m[3][1] = 0.0;
	pmatPersp->m[3][2] = -c.f * c.n / (c.f - c.n);
	pmatPersp->m[3][3] = 0.0;
}

void MatrixRotateY(Mat4 *pMatrix, float angleDeg)
{
	float theta = ToRad(angleDeg);
	pMatrix->m[0][0] = cos(theta);
	pMatrix->m[0][1] = 0;
	pMatrix->m[0][2] = -sin(theta);
	pMatrix->m[0][3] = 0;
	pMatrix->m[1][0] = 0;
	pMatrix->m[1][1] = 1;
	pMatrix->m[1][2] = 0;
	pMatrix->m[1][3] = 0;
	pMatrix->m[2][0] = -pMatrix->m[0][2];
	pMatrix->m[2][1] = 0;
	pMatrix->m[2][2] = pMatrix->m[0][0];
	pMatrix->m[2][3] = 0;
	pMatrix->m[3][0] = 0;
	pMatrix->m[3][1] = 0;
	pMatrix->m[3][2] = 0;
	pMatrix->m[3][3] = 1;
}

void MatrixRotateZ(Mat4 *pMatrix, float angleDeg)
{
	float theta = ToRad(angleDeg);
	pMatrix->m[0][0] = cos(theta);
	pMatrix->m[0][1] = sin(theta);
	pMatrix->m[0][2] = 0;
	pMatrix->m[0][3] = 0;
	pMatrix->m[1][0] = -pMatrix->m[0][1];
	pMatrix->m[1][1] = pMatrix->m[0][0];
	pMatrix->m[1][2] = 0;
	pMatrix->m[1][3] = 0;
	pMatrix->m[2][0] = 0;
	pMatrix->m[2][1] = 0;
	pMatrix->m[2][2] = 1;
	pMatrix->m[2][3] = 0;
	pMatrix->m[3][0] = 0;
	pMatrix->m[3][1] = 0;
	pMatrix->m[3][2] = 0;
	pMatrix->m[3][3] = 1;
}

void MatrixRotateAxis(Mat4 *pMatrix, Vec4 axis, float angleDeg) {}

//______________________________________________________________________________
// PIXEL SHADER
void inline WritePixel(ScreenBuffer *pBuffer, uint i, uint j, Color color)
{
	if (i < gViewWidth && j < gViewHeight)
	{
		uint32 *pixel =
			(uint32 *)pBuffer->pMemory + (j * pBuffer->pitch / 4) + i;
		*pixel = (color.r << 16) | (color.g << 8) | color.b;
	}
}

//______________________________________________________________________________
// DATA INPUT
bool ReadOFFModel(const char *filename, Model *pModel)
{
	// Open the file
	FILE *pFile;
	fopen_s(&pFile, filename, "r");
	if (pFile == 0)
	{
		return false;
	}
	char buffer[1024];
	// Check that the file is an OFF file
	fgets(buffer, 1024, pFile);
	if (feof(pFile) || strncmp(buffer, "OFF\n", 4))
	{
		return false;
	}
	// Read file header
	fgets(buffer, 1024, pFile);
	if (feof(pFile))
	{
		return false;
	}
	else
	{
		sscanf_s(buffer, "%d%d%d", &pModel->nVerts, &pModel->nFaces,
				 &pModel->nEdges);
	}

	// Read all vertices
	pModel->minX = FLT_MAX;
	pModel->minY = FLT_MAX;
	pModel->minZ = FLT_MAX;
	pModel->maxX = FLT_MIN;
	pModel->maxY = FLT_MIN;
	pModel->maxZ = FLT_MIN;
	Vec4 *aVerts = new Vec4[pModel->nVerts];
	for (uint t = 0; t < pModel->nVerts; ++t)
	{
		fgets(buffer, 1024, pFile);
		sscanf_s(buffer, "%f%f%f", &(aVerts[t].x), &(aVerts[t].y),
				 &(aVerts[t].z));
		aVerts[t].w = 1.0;
		if (aVerts[t].x > pModel->maxX)
		{
			pModel->maxX = aVerts[t].x;
		}
		if (aVerts[t].x < pModel->minX)
		{
			pModel->minX = aVerts[t].x;
		}
		if (aVerts[t].y > pModel->maxY)
		{
			pModel->maxY = aVerts[t].y;
		}
		if (aVerts[t].y < pModel->minY)
		{
			pModel->minY = aVerts[t].y;
		}
		if (aVerts[t].z > pModel->maxZ)
		{
			pModel->maxZ = aVerts[t].z;
		}
		if (aVerts[t].z < pModel->minZ)
		{
			pModel->minZ = aVerts[t].z;
		}
	}
	pModel->pVerts = aVerts;

	// Read all tri/quad faces and triagulate quads
	Tri *aTris = new Tri[pModel->nFaces];
	Quad *aQuads = new Quad[pModel->nFaces];
	uint iTri = 0;
	uint iQuad = 0;
	for (uint t = 0; t < pModel->nFaces; ++t)
	{
		fgets(buffer, 1024, pFile);
		uint nFaceVerts;
		sscanf_s(buffer, "%d", &nFaceVerts);
		if (nFaceVerts == 3)
		{
			sscanf_s(buffer, "%d%d%d%d", &nFaceVerts, &(aTris[iTri].a),
					 &(aTris[iTri].b), &(aTris[iTri].c));
			++iTri;
		}
		else if (nFaceVerts == 4)
		{
			sscanf_s(buffer, "%d%d%d%d%d", &nFaceVerts, &(aQuads[iQuad].a),
					 &(aQuads[iQuad].b), &(aQuads[iQuad].c),
					 &(aQuads[iQuad].d));
			++iQuad;
		}
	}
	pModel->nTris = iTri + iQuad * 2;
	Tri *aTriList = new Tri[pModel->nTris];
	for (uint t = 0; t < iTri; ++t)
	{
		aTriList[t] = aTris[t];
	}
	// Triangulate quads
	for (uint t = 0; t < iQuad; ++t)
	{
		aTriList[iTri + 2 * t].a = aQuads[t].a;
		aTriList[iTri + 2 * t].b = aQuads[t].b;
		aTriList[iTri + 2 * t].c = aQuads[t].c;
		aTriList[iTri + 2 * t + 1].a = aQuads[t].c;
		aTriList[iTri + 2 * t + 1].b = aQuads[t].d;
		aTriList[iTri + 2 * t + 1].c = aQuads[t].a;
	}
	pModel->pTris = aTriList;

	// Free memory and close file
	delete[] aTris;
	delete[] aQuads;
	fclose(pFile);
	return true;
}